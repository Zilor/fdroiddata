מקלדת חלופית על גבי המסך למגוון שפות.

אנו ננסה לפרסם כל חבילת שפה או ערכת עיצוב שיש להן את מקור ה־XML וייחוס/רישיון למילונים. למידע נוסף יש לגשת אל [http://f-droid.org/forums/topic/anysoftkeyboard-language-packs-layouts-plans/#post-6408 הנושא בפורום] של f-droid.org.
